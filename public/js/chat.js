var socket;
var messageField = document.getElementById('message-to-send');
var chatDialog = document.getElementById('chat-dialog');
var attachmentDialog = document.getElementById('attachment-dialog');
var registerDialog = document.getElementById('register-dialog');
var chatHistory = document.getElementById('chat-history');
var chatContainer = document.getElementById('chat-history-container');
var nickname = "";
var userListElement;
var userId="";

// init bunch of sounds
ion.sound({
    sounds: [
        {name: "beer_can_opening"},
        {name: "bell_ring"},
        {name: "branch_break"},
        {name: "button_click"},
        {name: "water_droplet"}
    ],

    // main config
    path: "./assets/js/sounds/",
    preload: true,
    multiplay: true,
    volume: 0.1
});

//Chat and socket setup
window.addEventListener('load', setup(), false )



function loadHistory(){
    
    socket.emit('loadHistory',"",function (data) { // args are sent in order to acknowledgement function
        
        Object.keys(data).forEach(function(key){
            msg = data[key];
            if(msg['author_id'] != getCookie('user_id')){ // Not sender messages
                
                if(msg['media_url'] != undefined && msg['media_url'] != ""){//Media
                    chatHistory.innerHTML +=  '<li><div class="message other-message"><img class="msg-image" src="'+ msg['media_url'] +'"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
                else{// Text message
                    chatHistory.innerHTML +=  '<li><div class="message other-message"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
            
            }
            else{ // Sender messages
                if(msg['media_url'] != undefined && msg['media_url'] != ""){// Media
                    chatHistory.innerHTML +=  '<li class="my-msg-element"><div class="message my-message"><img class="msg-image" src="'+ msg['media_url'] +'"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                    console.log('mio messaggio media');
                }
                else{// Text message
                    chatHistory.innerHTML +=  '<li class="my-msg-element"><div class="message my-message"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+msg["author"]+'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
            } 
            chatContainer.scrollTop = chatContainer.scrollHeight;
        });
    })
};


messageField.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        sendMsg();
    }
});

function sendMsg() {
    if(document.getElementById('message-to-send').value != ""){
        var msg = {"author_id":getCookie('user_id'), "author":getCookie('nick_cookie'), "content":document.getElementById('message-to-send').value};
        console.log("messaggio");
        console.log(msg);
        socket.emit('sendMsg', msg, function (data) { // args are sent in order to acknowledgement function
            document.getElementById('message-to-send').value = "";
        });
    }
}

document.getElementById('log-nickname').addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        setNickname();
    }
});

// Close dialog button
$('.dialog-top-border').click(function(){
    $(this).closest("div.dialog").hide();
    $('.page').removeClass('blurred');
});

function setup(){
    /* Join the chat and create socket */
        //socket = io('',{ query: 'nickname='+this.nickname});
        socket = io(window.location.href || SOCKET_HOST);

        socket.on('connect',function(){
            
            // Check token validity (TODO expiration)
            var token = getCookie('session_token');
            var nick_cookie = getCookie('nick_cookie'); 
            
            socket.emit('check_token', token, nick_cookie, function(data){
                if(data['type']=='error'){
                    console.log('Necessaria nuova autenticazione');
                    chatDialog.style.display = 'block';
                    
                }
                else{ // Autenticazione ok
                    joinChat();
                    // Bind Socket id to user_id
                    socket.emit('bind_socket_id',getCookie('user_id'),socket.id);
                    console.log("Autenticazione ok");
                }
            });

        /* Setup socket events */
        socket.on('updateUsers',function(userMap_json){
            console.log('newUser arrived');
            var userMap = new Map(JSON.parse(userMap_json))
            console.log(userMap);
            updateUsers(userMap);
        });
        
        function updateUsers(userMap){
                // play sound
                ion.sound.play("bell_ring");
                userListElement = document.createElement('div');
                console.log('usermappa');
                console.log(userMap);
                
                for(var [user_id, nickname] of userMap){
                    if(nickname != getCookie('nick_cookie')){
                        userListElement.innerHTML += '<div class="user-list" style="color:'+ getRandomColor() +'">'+ nickname+'</div>'+', ';
                    }
                }
                /* Workaraound to clean innerhtml */
                document.getElementById('users-online').innerHTML = "";
                document.getElementById('users-online').appendChild(userListElement);
        }
        
        socket.on('leftUser', function(data, res){
            console.log('UserLeft');
            console.log(data);
            // play sound
            ion.sound.play("branch_break");
            this.userList = data;
            userListElement = document.createElement('div');
            Object.keys(data).forEach(element => {
                if(element != nickname){
                    userListElement.innerHTML += '<div class="user-list" style="color:'+ getRandomColor() +'">'+ element +'</div>'+', ';
                    console.log('adding: '+element);
                }
            });
            /* Workaraound to clean innerhtml */
            document.getElementById('users-online').innerHTML = "";
            document.getElementById('users-online').appendChild(userListElement);
        });

        /* Receive messages */
        socket.on('recMsg',function(msg, res){
            console.log('recMsg');
            console.log(msg);
            // play sound
            ion.sound.play("beer_can_opening");

            if(msg['author_id'] != getCookie('user_id')){ // Not sender messages
                
                if(msg['media_url'] != undefined && msg['media_url'] != ""){//Media
                    chatHistory.innerHTML +=  '<li><div class="message other-message"><img class="msg-image" src="'+ msg['media_url'] +'"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
                else{// Text message
                    chatHistory.innerHTML +=  '<li><div class="message other-message"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
            
            }
            else{ // Sender messages
                if(msg['media_url'] != undefined && msg['media_url'] != ""){// Media
                    chatHistory.innerHTML +=  '<li class="my-msg-element"><div class="message my-message"><img class="msg-image" src="'+ msg['media_url'] +'"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ msg["author"] +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                    console.log('mio messaggio media');
                }
                else{// Text message
                    chatHistory.innerHTML +=  '<li class="my-msg-element"><div class="message my-message"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+msg["author"]+'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
                }
            }   
            chatContainer.scrollTop = chatContainer.scrollHeight;
        });
    });
}

function joinChat(){
    
    // Hide dialog
    chatDialog.style.display = 'none';
  
    $('.page').removeClass('blurred');

    // Set nickname
    $('#nick-label').prepend('Ciao, <b>' +getCookie('nick_cookie')+'</b>');

    // Load chat history
    loadHistory();
}

function login(){

    var nickname = $('#log-nickname').val();
    var pwd = $('#log-pwd').val();

    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(PUBLIC_KEY);
    var encrypted = encrypt.encrypt(pwd);

    socket.emit('login',nickname,encrypted,function(response){
    if(response['type']=="success"){

        // Save token to browser session
        document.cookie = 'session_token='+response['token']+';';    
        document.cookie = 'nick_cookie='+response['nickname']+';';
        document.cookie = 'user_id='+response['user_id']+';';
        console.log('Cookie are set');

        // Bind socket id to user_id
        socket.emit('bind_socket_id',getCookie('user_id'),socket.id);

        joinChat();
    }  
    else{
        console.log(response['message']);
    }

    });

}

function setNickname(nickname){
    if(!document.getElementById('nickname').validity.valueMissing){
        this.nickname = document.getElementById('nickname').value;
        chatDialog.style.display = "none";
        document.getElementById('nick-label').prepend('Ciao, '+this.nickname);
        console.log('nickname: '+this.nickname);
        joinChat();
    }
    else{
        document.getElementById('nickname').setCustomValidity("Invalid field.");
    }
}

document.getElementById('log-nickname').addEventListener("keydown",function(event){
    this.setCustomValidity("");
});

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

function showNickDialog(){
    $('.page').addClass('blurred');
    chatDialog.style.display = "block";
};

function openAttachmentDialog(image){

    $('.page').addClass('blurred');
    attachmentDialog.style.display = "block";

};

function openRegisterDialog(){
    //(document.getElementsByClassName('page')[0]).classList.toggle('blurred');
    chatDialog.style.display = "none";
    registerDialog.style.display = "block";
    
};

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

function registerLogin(){
    if ($('#pwd-reg').val() != $('#pwd-reg-check').val()) {
        $('#pwd-reg-check').get(0).setCustomValidity('Password don\'t match');
        $('#pwd-reg-check').get(0).reportValidity();
        console.log("pwd non match");
    } else if($('#nick-reg').val() != "") {
        // input is valid -- reset the error message
        $('#pwd-reg')[0].setCustomValidity('');
        console.log("pwd match");
    
        // MD5 password
        var md5 = (CryptoJS.MD5($('#pwd-reg').val())).toString();
        console.log(md5);
        var user = {
            'user_id': (Math.random()*1e32).toString(36) ,
            'nickname':$('#nick-reg').val(), 
            'md5': md5,
            'token': "",
            'token_validity':""
        }
        
        socket.emit('registeredUser',user,function(data){
            console.log(data);
            if(data['type']=='error'){
                $('#reg-error').html(data['message']);
            }
            else{
                // Save token to browser session
                document.cookie = 'session_token='+data['token']+';';    
                document.cookie = 'nick_cookie='+user['nickname']+';';           
                joinChat(user['id']);
            }
        });

        console.log('emitted registration');
    }
}

function previewImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#image-preview').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadImage(){
    
    data = new FormData();

    data.append('photo', $('#image-file')[0].files[0]);
    data.append('content', $('#image-caption').val());
    data.append('nickname',getCookie('nick_cookie'));
    data.append('user_id',getCookie('user_id'));
    
    $.ajax({
        type:'POST',
        url: './upload',
        data: data,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            console.log("success");
            console.log(data);
        },
        error: function(data){
            console.log("error");
            console.log(data);
        }
    });

    $('.page').removeClass('blurred');;
    attachmentDialog.style.display = "none";

    // Load message in history
    //chatHistory.innerHTML +=  '<li><div class="message other-message"><div class="message-content">' + msg["content"] + '</div><div class="message-info">'+ 'MEDIA' +'</span><span class="message-data-time">'+ msg['timestamp'] +'</span></div></div></li>';
}
