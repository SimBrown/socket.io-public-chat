var express = require('express');
var path = require('path');
const fsExtra = require('fs-extra');
var crypto_js = require("crypto-js");
var date = new Date();
var app = express();
const NodeRSA = require('node-rsa');
const key = new NodeRSA("-----BEGIN RSA PRIVATE KEY-----\n"+
"MIICXAIBAAKBgQDPpjMYlBgaj5Sw2hgm4uSvJLpwJGMKEtgindZKJsz5RFokz6sn\n"+
"1F43PG9oWj21Duv8vPKe+O4C8jBNnZS6Xxabi4pKmsHjFf1zabeAPoQE1KHd5T+n\n"+
"UNE4LPAtdrLrj5lCdYIkjXDTGN0zowFuPTy12/LJKfnVT20bOankDJIfqwIDAQAB\n"+
"AoGBAIBkGS/XUdgKczuufkR9Aj0xDn4MYZh28mDlCvyqZQ6zLrGYHhHwg5jPq2rw\n"+
"cTskUNHi2nllHXWA3lD/YFikNss0loZQq8rVmbIL7aA4yRPaRoAxSGikGXh18523\n"+
"WB8cMzp+9JFmXTPqpIpP3yPSVg2a1dfwYb1c8mddDX09saDBAkEA9cwp3k6q5Fe5\n"+
"kj9T4E4S8Xhp60Nn/p9d/zScTcItvpCmS3mbwraDFKSf7r/FiooTvCkjKc9UFDRh\n"+
"u58/9CEiMQJBANhErGjkthGZ88aqsz7Ha+ihdVerv+FP22uoKV559LbNDmwe8mCt\n"+
"b4RM3OjL9wBKcc1IrU1k1IFI5UOJHdpCLJsCQCCXi82wfLR+dVrRDtwlPhnKr7DE\n"+
"ZVu74gje/wLIhqxIHeTQITk2Xjc0hQhxYU43dNp0UfdPSITNVVA9k4tZgWECQAlX\n"+
"lhcF4jOKcfRgBVQt6yRmbRo3aheCipjw77CMFh0baIcVIyU3+Lw/Ub+gp+/UR2w1\n"+
"un7AX0mia3zqXGbEOwcCQHV9g6eXtvbtwhipZe0Q/7cw9e91Guatk6EUVTHqWz3t\n"+
"WJY7UBz6Jk8H02WqFu/Z8JR8CJ0mUanQ+NqVPLFODa4=\n"+
"-----END RSA PRIVATE KEY-----");

/** Setting up environment variables  */

require('dotenv').config()

/** Db setup */
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

/** Libraries for storage */
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/'});

/** Body parser for parsing requests data */
const bodyParser = require("body-parser");

var nickname = "";
var userID;
var userMap = new Map();

// Set some defaults (required if your JSON file is empty)
db.defaults({ messages: [], users: [] }).write()// Save a message to db

/** Listening to a port */
var server = app.listen(process.env.PORT || 8000, function(){
    console.log('Node app is listening on port 8000');
    console.log('Running on: '+ process.env.APP_HOST);
});

/** Build a socket on the server */
const io = require('socket.io').listen(server);

/** List of active users in chat */
var userList = {};

/** Offer static resources */
app.use('/assets/',express.static('public/'));
app.use('/media/',express.static('uploads/'));

app.use(bodyParser.json());

/** Routes */
app.get('/', function(req,res){
    res.sendFile(path.join(__dirname + '/public' + '/home.html'));
});

app.post('/deleteDb',function(req,res){
    db.set('messages', [])
    .write()

    fsExtra.emptyDirSync('./uploads');
    res.send('Messages deleted');
})


/** Socket Behaviour */
io.on('connection', function(socket){

    /** Upload media */
    app.post('/upload', upload.single('photo'), (req, res) => {

        // Get fresh time
        var date = new Date();

        console.log(req);

        msg = {
            "author_id": req.body.user_id,
            "author": req.body.nickname,
            "content": req.body.content,
            "timestamp": date.getHours()+':'+ date.getMinutes() +', Oggi',
            "media_url": '/media/'+ req.file.filename
        }

        if(req.file) {
            res.json(msg);
        }
        else throw 'error';

        // Saving message to db/
        db.get('messages')
        .push({
                "author_id": msg['author_id'] ,
                "author": msg['author'],
                "content": msg['content'],
                "timestamp": msg['timestamp'],
                "media_url": msg['media_url']
        })
        .write();
        console.log(msg);

        // Send message to all clients
        io.sockets.emit('recMsg', msg);
    });
    /** End Routes */

    // User leaves the chat
    socket.on('disconnect',function(data){
        // Remove user from activeUserList
        console.log("Client "+socket.id+" disconnected from backend");
        
        var user = db.get('users').filter({socket_id:socket.id}).value()[0];

        if(user){userMap.delete(user.user_id)};

        io.sockets.emit('updateUsers',JSON.stringify(Array.from(userMap)));

    });

    // Give timestamp to new message, saves and sends to all clients
    socket.on('sendMsg',function(data,fun){
        date = new Date();
        data['timestamp'] = date.getHours()+':'+ date.getMinutes() +', Oggi'
        fun(data['timestamp']);

        // Saving message to db/
        db.get('messages')
        .push({ author_id: socket.id ,author: nickname, content: data['content'], timestamp: data['timestamp'], media_url: ""})
        .write();

        console.log('messaggio salvato');
        console.log(data);
        io.sockets.emit('recMsg', data);
    });

    // Load history
    socket.on('loadHistory',function(data,fun){
        var result = db.get('messages').filter({}).value();
        console.log('Loading history');
        fun(result);
    });

    // User Login
    socket.on('login',function(nickname,pwd,fun){
        var response = {};
        console.log('login');
        //Decript password and hash
        console.log(pwd);
        key.setOptions({encryptionScheme: 'pkcs1'});
        var decrypted = key.decrypt(pwd, 'utf8');

        console.log(decrypted)
        // Hashed pwd
        var hash = crypto_js.MD5(decrypted).toString();
        console.log(hash);

        // Check password validity
        if(db.get('users').filter({'nickname':nickname,'md5':hash}).value().length){
            response['type'] = 'success';
            response['message'] = 'User logged in!';
            response['nickname'] = nickname;
            response['token'] = (Math.random()*1e32).toString(36);

            // Set token into db for user
            db.get('users').find({'nickname':nickname})
            .assign({"token":response['token']}).value();
            // Must write edits
            db.write();

            var result = db.get('users').find({'nickname':nickname}).value();
            var user = {'user_id':result['user_id'],'nickname':result['nickname']};

            response['user_id'] = user.user_id;
            
            // Add user to map
            userMap.set(user.user_id, user.nickname);
            
            // Must send response to client to set cookies properly before emit
            fun(response);
            
            io.sockets.emit('updateUsers',JSON.stringify(Array.from(userMap)));

        }
        else{
            response['type'] = 'error';
            response['message'] = 'Access Denied (Wrong pwd or Username)';

            return fun(response)
        }

    });

    // Register user
    socket.on('registeredUser',function(data,fun){
        console.log(db.get('users').filter({'nickname':data['nickname']}).value().length);
        var response = {}
        // Nickname already present
        if(db.get('users').filter({'nickname':data['nickname']}).value().length){
            response['type'] = 'error';
            response['message'] = 'User Already Exist!';
            return fun(response)
        }
        else{
            db.get('users')
            .push({
                'user_id': data['user_id'],
                'nickname':data['nickname'],
                'md5': data['md5'],
                'token': data['token'],
                'token_validity': data['token_validity'],
            })
            .write();

            response['type'] = 'success';
            response['message'] = 'User Added!';
            response['token'] = (Math.random()*1e32).toString(36);

            console.log('Registered user');
            return fun(response);
        }
    });
    
    // Update users
    socket.on('updateUsers',function(user_id){
        userMap.delete(user_id);
        io.sockets.emit('updateUsers', JSON.stringify(Array.from(userMap)));
        console.log('userRemoved');
    })
    
    // Update users
    socket.on('bind_socket_id',function(user_id,socket_id){
        // Set token into db for user
        db.get('users').find({'user_id':user_id})
        .assign({"socket_id":socket_id}).value();
        // Must write edits
        db.write();

        console.log('socket id binded');
    })

    // Check user Token
    socket.on('check_token',function(token,nickname,fun){
        var response = {}
        console.log('Checking token');
        if(db.get('users').filter({'nickname':nickname,'token':token}).value().length == 1){
            response['type'] = 'success';
            response['message'] = 'token is valid';
            var user = (db.get('users').filter({'nickname':nickname}).value())[0];
            //Emit user list to clients
            console.log('User is returned to session');
            console.log(user);
            
            fun(response);
            
            // Add user to map
            userMap.set(user.user_id, user.nickname);
        
            io.sockets.emit('updateUsers', JSON.stringify(Array.from(userMap)));

        }
        else{
            response['type'] = 'error';
            response['message'] = 'token is invalid';
            return fun(response);
        }
    })

});
/** End socket behaviour */
